# BlockPower Contact Retrieval

## Project Setup

This is a Node project written in Typescript. To finish setup, you need to

1. Install dependencies: `$ npm install`
2. Compile the Typescript to JavaScript: `$ npm run tsc`

If you have a good IDE, you can set it up to automatically keep track of Typescript changes to regenerate
the corresponding JavaScript files. 

## Google

To retrieve the contacts from Google, do the following

1. Run `$ node ./src/google/retrieve_google_contacts.js`
2. Go to the printed URL in a browser and run through the setup
3. Go to `https://hookbin.com/zrodyzK82JfykkGKwl2Y` and retrieve the latest `code` value from the query string section
4. Paste the `code` value into the `token_retrieval_code` field in `src/google/google_config.json`
5. Run `$ node ./src/google/retrieve_google_contacts.js` again
