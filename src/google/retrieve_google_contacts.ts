import * as fs from 'fs';
import {google} from 'googleapis';
import debugModule = require('debug');

const debug = debugModule('blockpower-contact-retrieval:google');

const config = require('./google_config.json');

// If modifying these scopes, delete token.json.
const SCOPES = config.scopes;
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = `${__dirname}/token.json`;

const CLIENT_ID = config.client_id;
const CLIENT_SECRET = config.client_secret;
const redirectUri = config.redirect_uri;

(async () => {
	const oAuth2Client = new google.auth.OAuth2(CLIENT_ID, CLIENT_SECRET, redirectUri);

	try {
		// we have a token file
		const tokenJSON = fs.readFileSync(TOKEN_PATH);
		const token = JSON.parse(tokenJSON.toString('utf-8'));
		oAuth2Client.setCredentials(token.tokens);

	} catch (e) {
		const code = config.token_retrieval_code;
		if (code) {
			// @ts-ignore
			const token = await oAuth2Client.getToken(code);
			debug('Token: %O', token)
			fs.writeFileSync(TOKEN_PATH, JSON.stringify(token, null, 4));

			oAuth2Client.setCredentials(token.tokens);
		} else {
			// we do not have a token file nor a token retrieval code
			const authUrl = oAuth2Client.generateAuthUrl({
				access_type: 'offline',
				scope: SCOPES
			});
			console.log('Authorize this app by visiting this url:', authUrl);
			console.log(`Then, visit https://hookbin.com/zrodyzK82JfykkGKwl2Y and copy the code value from the query string section into the token_retrieval_code field in google_config.json`);
			console.log('Once you\'ve copied the value, rerun the script.');

			return;
		}
	}

	const service = google.people({version: 'v1', auth: oAuth2Client});
	let contacts = [];
	let nextPageToken = null;
	do {
		const currentList = await service.people.connections.list({
			resourceName: 'people/me',
			pageSize: 1000,
			personFields: config.contact_fields.join(','),
			pageToken: nextPageToken
		});
		nextPageToken = currentList.data.nextPageToken;

		const delta = currentList.data.connections;
		contacts = [...contacts, ...delta];
	} while (nextPageToken);

	console.dir(contacts);

})();

